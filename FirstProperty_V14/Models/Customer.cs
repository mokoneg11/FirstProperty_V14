﻿using System;
namespace FirstProperty_V14.Models
{
    public class Customer
    {
        private string v;
        private int v1;
        private string v2;
        private string v3;
        private string v4;
        private string v5;

        public int Customer_Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Contact { get; set; }
        public string Gender { get; set; }

        public Customer(string email, string password, string firstname, string lastname)
        {
            Email = email;
            Password = password;
            Firstname = firstname;
            Lastname = lastname;
        }

		public Customer()
		{
			
		}
		public Customer(int ID, string email, string password, string firstname, string lastname, string phone, string gend)
		{
            Customer_Id = ID;
			Email = email;
			Password = password;
			Firstname = firstname;
			Lastname = lastname;
            Contact = phone;
            Gender = gend;
		}
		public Customer( string email, string password, string firstname, string lastname, string phone, string gend)
		{
			Email = email;
			Password = password;
			Firstname = firstname;
			Lastname = lastname;
			Contact = phone;
			Gender = gend;
		}
        public Customer(string emails,string password) 
        {
            Email = emails;
            Password = password;
        }

        public Customer(int v1, string v2, string v3, string v4, string v5)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
            this.v4 = v4;
            this.v5 = v5;
        }

        public Customer(string email, string password, string firstname, string lastname, string v) : this(email, password, firstname, lastname)
        {
            this.v = v;
        }

        public Customer(int v1, string v2, string v3, string v4, string v5, string v) : this(v1, v2, v3, v4, v5)
        {
            this.v = v;
        }
    }
}
