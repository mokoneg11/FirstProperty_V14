﻿using System;
namespace FirstProperty_V14.Models
{
    public class EmailSend
    {
        
        public string senderName { get; set; }
        public string senderEmail {get; set;}
        public string senderContact { get; set; }
        public string message { get; set; }
          
        public EmailSend(string sName, string sEmail, string sPhone, string Enquire)
        {
            senderName = sName;
            senderEmail = sEmail;
            senderContact = sPhone;
            message = Enquire;
        }
        public EmailSend()
        {
        }
    }
}
