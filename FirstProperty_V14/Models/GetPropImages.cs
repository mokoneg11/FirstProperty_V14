﻿using System;
namespace FirstProperty_V14.Models
{
    public class GetPropImages
    {
		public int Prop_Id { get; set; }
		public string Property_Desc { get; set; }
		public string Property_Type { get; set; }
		public string Property_Stat { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public int BedRoom { get; set; }
		public int BathRoom { get; set; }
		public int Garage { get; set; }
		public double Price { get; set; }
		public int Imgae_Id { get; set; }
		public string Name { get; set; }
		public byte[] Images { get; set; }
		public int Prop_ID { get; set; }

        public GetPropImages(int pId, string p_desc,string p_type,string p_stat,string addr, string city, int bed, int bath, int gar, double amnt,int imgId,string nam, byte[] imgs, int propie)
        {
            Prop_Id = pId;
            Property_Desc = p_desc;
            Property_Type = p_type;
            Property_Stat = p_stat;
            Address = addr;
            City = city;
            BedRoom = bed;
            BathRoom = bath;
            Garage = gar;
            Price = amnt;
            Imgae_Id = imgId;
            Name = nam;
            Images = imgs;
            Prop_ID = propie;
        }

        public GetPropImages()
        {
        }
    }
}
