﻿using System;
using System.IO;
using MySql.Data.MySqlClient;

namespace FirstProperty_V14.Models
{
    public class PropImage
    {
        private int v;

        public int Imgae_Id { get; set; }
        public string Name { get; set; }
        public byte[] Images { get; set; }
        public int Prop_ID { get; set; }


        public PropImage(int img_id , string name, byte[]img, int prop)
        {
            Imgae_Id = img_id;
            Name = name;
            Images = img;
            Prop_ID = prop;
        }

     
        public PropImage()
        {
        }

    }
}
