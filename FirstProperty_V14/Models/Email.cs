﻿using System;
namespace FirstProperty_V14.Models
{
    public class Email
    {
        public string eName;
        public string eFrom;
        public string eNumber;
        public string eBody;

        public string EmailName
        {
            get { return eName; }
            set { eName = value; }
        }
        public string EmailFrom
        {
            get { return eFrom; }
            set { eFrom = value; }
        }
        public string EmailNumber
        {
            get { return eNumber; }
            set { eNumber = value; }
        }
        public string EmailBody
        {
            get { return eBody; }
            set { eBody = value; }
        }
        public Email()
        {

        }

    }
}
