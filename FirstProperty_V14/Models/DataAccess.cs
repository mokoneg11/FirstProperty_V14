﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Data;

namespace FirstProperty_V14.Models
{
    public class DataAccess
    {
        static string connectString = "SERVER=localhost;UID=root;DATABASE=PropertyPortal;";
		static MySqlDataReader read;

        //Register
        public string  InsertUser(Customer cust)
		{
			string x = "";
			using (MySqlConnection connect = new MySqlConnection())
			{
				connect.ConnectionString = connectString;
                string query = "INSERT INTO PropertyPortal.Customers(Firstname,Lastname,Email,Password) " +
                    "VALUES('" + cust.Firstname + "','" + cust.Lastname + "','" + cust.Email + "','" + cust.Password + "');";
				using (MySqlCommand comma = new MySqlCommand(query, connect))
				{
					try
					{
						comma.Connection.Open();

						comma.Parameters.AddWithValue("@Firstname", cust.Firstname);
						comma.Parameters.AddWithValue("@Lastname", cust.Lastname);
						comma.Parameters.AddWithValue("@Email", cust.Email);
						comma.Parameters.AddWithValue("@Password", cust.Password);
						int y = comma.ExecuteNonQuery();

						x = y.ToString();

					}
					catch (MySqlException ex)
					{
						ex.ToString();
						comma.Connection.Close();
					}
				}
                return null;
			}
		}

        //Login
        public Customer CustomerLogin(string email, string passwords)
        {
            string sql = "SELECT Customer_Id,Firstname,Lastname,Email,Password,Contact,Gender FROM PropertyPortal.Customers WHERE Email='" + email + "'AND Password='" + passwords + "';";

            using (MySqlConnection connect = new MySqlConnection())
            {
                connect.ConnectionString = connectString;
                MySqlCommand comma = new MySqlCommand(sql, connect);
                comma.Connection = connect;

                try
                {
                    comma.Connection.Open();
                    comma.Parameters.Add(new MySqlParameter("@Email", email));
                    comma.Parameters.Add(new MySqlParameter("@Password", passwords));

                    read = comma.ExecuteReader();

                    while (read.Read())
                    { 
                        return new Customer(Convert.ToInt32(read["customer_id"]),Convert.ToString(read["email"]), Convert.ToString(read["password"]), Convert.ToString(read["firstname"]), Convert.ToString(read["lastname"]), Convert.ToString(read["contact"]), Convert.ToString(read["gender"]));
                    }
                    read.Close();
                }
                catch (MySqlException exception)
                {
                    comma.Connection.Close();
                    exception.ToString();
                }
                return null;
            }
        }

        //Get All The Customers
		public Customer[] GetAllCust()
		{
            string sql = "SELECT * FROM PropertyPortal.Customers;";
            List<Customer> clients = new List<Customer>();
			using (MySqlConnection connect = new MySqlConnection())
			{
				connect.ConnectionString = connectString;
               
				MySqlCommand comma = new MySqlCommand(sql, connect);
				comma.Connection = connect;
				try
				{
					comma.Connection.Open();
					Customer cust = new Customer();
                    //comma.Parameters.Add(new MySqlParameter("@Email", cust.Email));
					read = comma.ExecuteReader();
                    while (read.Read()){
                        cust = new Customer(Convert.ToInt32(read["Customer_Id"]),
                                            Convert.ToString(read["Email"]),
                                            Convert.ToString(read["Password"]),
                                            Convert.ToString(read["Firstname"]),
                                            Convert.ToString(read["Lastname"]),
                                            Convert.ToString(read["Contact"]),
                                            Convert.ToString(read["Gender"])
                                            );
                        clients.Add(cust);
					}
					read.Close();

					MySqlDataReader reader = comma.ExecuteReader(System.Data.CommandBehavior.SingleRow);
					reader.Read();
					reader.Close();
				}
				catch (MySqlException exception)
				{
					comma.Connection.Close();
					exception.ToString();
				}
                return clients.ToArray();
			}   
		}

        //Update customer information
       public Customer CustomerUpdate(Customer cust , int id)
        {
            string sql = "UPDATE PropertyPortal.Customers SET Firstname='" + cust.Firstname + "',Lastname='" + cust.Lastname + "',Contact='" + cust.Contact + "',Gender='" + cust.Gender + "' WHERE Customer_Id=" + id + ";";
            using (MySqlConnection connect = new MySqlConnection())
            {
                connect.ConnectionString = connectString;
                using (MySqlCommand comma = new MySqlCommand(sql, connect)) { 

                comma.Connection = connect;
                try
                {
                    comma.Connection.Open();

                        comma.Parameters.Add(new MySqlParameter("@_Firstname", cust.Firstname));
                        comma.Parameters.Add(new MySqlParameter("@_Lastname", cust.Lastname));
                        comma.Parameters.Add(new MySqlParameter("@_Email", cust.Email));
                        comma.Parameters.Add(new MySqlParameter("@_Password", cust.Password));
                        comma.Parameters.Add(new MySqlParameter("@_Contact", cust.Contact));
                        comma.Parameters.Add(new MySqlParameter("@_Gender", cust.Gender));

                    read = comma.ExecuteReader();
                    while (read.Read())
                    {
                            cust = new Customer(Convert.ToString(read["Firstname"]),
                                                Convert.ToString(read["Lastname"]),
                                                Convert.ToString(read["Email"]),
                                                Convert.ToString(read["Password"]),
                                                Convert.ToString(read["Contact"]),
                                                Convert.ToString(read["Gender"])
                                           );
                    }
                    read.Close();

                }
                catch (MySqlException exception)
                {
                    exception.ToString();

                }
                finally
                {
                    comma.Connection.Close();
                }
            }
                return cust;
            }
        }

		
        //Insert Property
		public string InsertProperty(Property prop)
		{
			string g = "";
			using (MySqlConnection connect = new MySqlConnection())
			{
				connect.ConnectionString = connectString;

                string querys = "INSERT INTO PropertyPortal.Property(Property_Desc,Property_Type,Property_Stat,Address,City,BedRoom,BathRoom,Garage,Price) " +
                    "VALUES('" + prop.Property_Desc + "','" + prop.Property_Type + "','" + prop.Property_Stat + "','" + prop.Address + "','" + prop.City + "','" + prop.BedRoom + "','" + prop.BathRoom + "','" + prop.Garage + "','" + prop.Price + "');";
				using (MySqlCommand comma = new MySqlCommand(querys, connect))
				{
                    comma.Connection = connect;
					try
					{
						comma.Connection.Open();

                        comma.Parameters.AddWithValue("@Property_Desc", prop.Property_Desc);
                        comma.Parameters.AddWithValue("@Property_Type", prop.Property_Type);
                        comma.Parameters.AddWithValue("@Property_Stat", prop.Property_Stat);
						comma.Parameters.AddWithValue("@Address", prop.Address);
						comma.Parameters.AddWithValue("@City", prop.City);
						comma.Parameters.AddWithValue("@BedRoom",prop.BedRoom);
                        comma.Parameters.AddWithValue("@BathRoom",prop.BathRoom);
                        comma.Parameters.AddWithValue("@Garage", prop.Garage);
						comma.Parameters.AddWithValue("@Price", prop.Price);
						int y = comma.ExecuteNonQuery();

						g = y.ToString();

					}
					catch (MySqlException ex)
					{
                        ex.ToString();
					}
                    finally{
                        comma.Connection.Close();
                    }
				}
			}
            return null;
		}

       /*Update property information*/
        public Property PropertyUpdate(int id, Property prope)
        {

            string sql = "UPDATE PropertyPortal.Property SET Property_Desc='" + prope.Property_Desc + "',Property_Type='" + prope.Property_Type + "',Property_Stat='" + prope.Property_Stat + "',Address='" + prope.Address + "',City='" + prope.City + "',BedRoom='" + prope.BedRoom + "',BathRoom='" + prope.BathRoom + "',Garage='" + prope.Garage + "',Price='" + prope.Price + "' WHERE Prop_Id='" + id + "';";
            using (MySqlConnection connect = new MySqlConnection())
            {
                connect.ConnectionString = connectString;
                using (MySqlCommand comma = new MySqlCommand(sql, connect))
                {

                    comma.Connection = connect;
                    try
                    {
                        comma.Connection.Open();

                        comma.Parameters.Add(new MySqlParameter("@Prop_Id", prope.Prop_Id));
                        comma.Parameters.Add(new MySqlParameter("@Property_Desc", prope.Property_Desc));
                        comma.Parameters.Add(new MySqlParameter("@Property_Type", prope.Property_Type));
                        comma.Parameters.Add(new MySqlParameter("@Property_stat", prope.Property_Stat));
                        comma.Parameters.Add(new MySqlParameter("@Address", prope.Address));
                        comma.Parameters.Add(new MySqlParameter("@City",prope.City));
                        comma.Parameters.Add(new MySqlParameter("@BedRoom", prope.BedRoom));
                        comma.Parameters.Add(new MySqlParameter("@BathRoom", prope.BathRoom));
                        comma.Parameters.Add(new MySqlParameter("@Garage", prope.Garage));
                        comma.Parameters.Add(new MySqlParameter("@Price", prope.Price));

                        read = comma.ExecuteReader();
                        while (read.Read())
                        {
                            prope = new Property(Convert.ToInt32(read["Prop_Id"]),
                                                Convert.ToString(read["Property_Desc"]),
                                                Convert.ToString(read["Property_Type"]),
                                                Convert.ToString(read["Property_Stat"]),
                                                Convert.ToString(read["Address"]),
                                                Convert.ToString(read["City"]),
                                                Convert.ToString(read["BedRoom"]),
                                                Convert.ToString(read["BathRoom"]),
                                                Convert.ToString(read["Garage"]),
                                                Convert.ToString(read["Price"])
                                                );
                        }                                  
                        read.Close();

                    }
                    catch (MySqlException exception)
                    {
                        exception.ToString();

                    }
                    finally
                    {
                        comma.Connection.Close();
                    }
                }
                return prope;
            }
        }
       
        /* Get All Property Information*/
        public Property[] GetProperty()
		{
			List<Property> properties = new List<Property>();

			using (MySqlConnection connect = new MySqlConnection())
			{
				connect.ConnectionString = connectString;

                string querys = "SELECT Prop_Id,Property_Desc,Property_Type,Property_Stat,Address,City,BedRoom,BathRoom,Garage,Price FROM PropertyPortal.Property;";
				using (MySqlCommand comma = new MySqlCommand(querys, connect))
				{
					try
					{
						comma.Connection.Open();
                        Property prop = new Property();

                        read = comma.ExecuteReader();
						while (read.Read())
						{
                            prop = new Property(Convert.ToInt32(read["Prop_Id"]),
                                                Convert.ToString(read["Property_Desc"]),
												Convert.ToString(read["Property_Type"]),
												Convert.ToString(read["Property_Stat"]),
												Convert.ToString(read["Address"]),
												Convert.ToString(read["City"]),
                                                Convert.ToString(read["BedRoom"]),
                                                Convert.ToString(read["BathRoom"]),
                                                Convert.ToString(read["Garage"]),
                                                Convert.ToString(read["Price"])
											   );
							properties.Add(prop);
							
						}
                        read.Close();
                        MySqlDataReader reader = comma.ExecuteReader(System.Data.CommandBehavior.SingleRow);
                        reader.Read();
                        reader.Close();
					}
					catch (MySqlException ex)
					{
						ex.ToString();
					}
                    finally{
                        comma.Connection.Close();
                    }
				}
                return properties.ToArray();
			}
			
		}

        /*Get All Properties that are for sale*/
		public Property[] SaleProperty(Property prop)
		{
            List<Property> properties = new List<Property>();
			string g = "";
			using (MySqlConnection connect = new MySqlConnection())
			{
				connect.ConnectionString = connectString;

                string querys = "SELECT * FROM PropertyPortal.Property WHERE Property_Status='For Sale';";
				using (MySqlCommand comma = new MySqlCommand(querys, connect))
				{
                    try
                    {
                        comma.Connection.Open();
                        Property prope = new Property();

                        read = comma.ExecuteReader();
                        while (read.Read())
                        {
                            prope = new Property(Convert.ToInt32(read["Prop_Id"]),
                                                 Convert.ToString(read["Property_Desc"]),
                                                 Convert.ToString(read["Property_Type"]),
                                                 Convert.ToString(read["Property_Stat"]),
                                                 Convert.ToString(read["Address"]),
                                                 Convert.ToString(read["City"]),
                                                 Convert.ToString(read["BedRoom"]),
                                                 Convert.ToString(read["BathRoom"]),
                                                 Convert.ToString(read["Garage"]),
                                                 Convert.ToString(read["Price"])
                                               );
                            properties.Add(prope);
                        }
                    }
					catch (MySqlException ex)
					{
						ex.ToString();
						comma.Connection.Close();
					}
				}
			}
            return properties.ToArray() ;
		}


        /*Adding an image to the database*/
        public string AddImage(Gallery gal)
        {
            string message = "";
            using (MySqlConnection connect = new MySqlConnection(connectString))
            {
                string query = "INSERT INTO PropertyPortal.Gallery(File_Name,FilePath) " +
                    "VALUES('" + gal.File_Name + "'," + gal.FilePath + ");";
                
                MySqlCommand comma = new MySqlCommand(query, connect);

                MySqlParameter[] paras = new MySqlParameter[2];

                paras[0] = new MySqlParameter("@File_Name", MySqlDbType.String);
                paras[0].Value = gal.File_Name;

                paras[1] = new MySqlParameter("@FilePath", MySqlDbType.MediumBlob);
                paras[1].Value = gal.FilePath;

                //paras[2] = new MySqlParameter("@Prop_Id", MySqlDbType.Int32);
                //paras[2].Value = img.Prop_ID;

                comma.Parameters.AddRange(paras);

                comma.Connection.Open();
                int x = comma.ExecuteNonQuery();

                        if (x > 0){
                           message = "Image/s saved successfully";
                           comma.Connection.Close();
                        }
                       else{
                          message = "Incomplete Data";
                       }
                comma.Connection.Close();

                return "Image Added";
            }
        }

        public string AddImage(PropImage img)
        {
            string message = "";
            using (MySqlConnection connect = new MySqlConnection(connectString))
            {
                string query = "INSERT INTO PropertyPortal.PropImages(Name,Images,Prop_Id) " +
                    "VALUES('" + img.Name + "', '@Images'," + img.Prop_ID + ");";

                MySqlCommand comma = new MySqlCommand(query, connect);

                MySqlParameter[] paras = new MySqlParameter[3];

                paras[0] = new MySqlParameter("@File_Name", MySqlDbType.String);
                paras[0].Value = img.Name;

                paras[1] = new MySqlParameter("@FilePath", MySqlDbType.MediumBlob);
                paras[1].Value = img.Images;

                paras[2] = new MySqlParameter("@Prop_Id", MySqlDbType.Int32);
                paras[2].Value = img.Prop_ID;

                comma.Parameters.AddRange(paras);

                comma.Connection.Open();
                int x = comma.ExecuteNonQuery();

                if (x > 0)
                {
                    message = "Image/s saved successfully";
                    comma.Connection.Close();
                }
                else
                {
                    message = "Incomplete Data";
                }
                comma.Connection.Close();

                return "Image Added";
            }
        }

        public string InsertImag(PropImage img)
        {
			string message = "";
            FileStream fs;
            BinaryReader br;
            using (MySqlConnection connect = new MySqlConnection())
            {
                
                string query = "INSERT INTO PropertyPortal.PropImages(Name,Images,Prop_ID) " +
                    "VALUES(@Name,@Images,@Prop_ID);";
                
				connect.ConnectionString = connectString;

				MySqlCommand comma = new MySqlCommand(query,connect);
                comma.Connection = connect;
                comma.Parameters.Add("_Name", MySqlDbType.VarChar,255);
                comma.Parameters.Add("_Images", MySqlDbType.MediumBlob);
                comma.Parameters.Add("_Prop_Id",MySqlDbType.Int32);

                comma.Parameters["Name"].Value = img.Name;
                comma.Parameters["Images"].Value = img.Images;
                comma.Parameters["Prop_ID"].Value = img.Prop_ID;

                comma.Connection.Open();
                int x = comma.ExecuteNonQuery();

                if (x > 0){
                   message = "Image/s saved successfully";
                    comma.Connection.Close();
                }
                else{
                   message = "Incomplete Data";
                }
                comma.Connection.Close();
                return message;
            }

        }

        /*Get All Images stored*/
        public PropImage[] GetImages()
        {
            string message = "";
            List<PropImage> images = new List<PropImage>();
			
            using (MySqlConnection connect = new MySqlConnection()){
                connect.ConnectionString = connectString;
                string query = "SELECT Image_Id,Name,Images,Prop_ID" +
                    " FROM PropertyPortal.ProImages";
                using(MySqlCommand comma = new MySqlCommand(query,connect)){
                    try{
                        comma.Connection.Open();
                        PropImage imgs;
                        while (read.Read())
						{
                            imgs = new PropImage(Convert.ToInt32(read["Imgae_Id"]),
                                                 Convert.ToString(read["Name"]),
                                                 (byte[])(read["Images"]),
                                                 Convert.ToInt32(read["Prop_ID"])
											    );
							images.Add(imgs);
						}
						read.Close();
						MySqlDataReader reader = comma.ExecuteReader(System.Data.CommandBehavior.SingleRow);
						reader.Read();
						reader.Close();
                    }catch(MySqlException ex){
                        message = ex.ToString();
                    }finally{
                        comma.Connection.Close();
                    }
                }
                return images.ToArray();
            }
        }

        /*Get all image and property information*/
        public GetPropImages[] GetAll()
        {
            List<GetPropImages> propImg = new List<GetPropImages>();
            using (MySqlConnection connect = new MySqlConnection()){
                connect.ConnectionString = connectString;
                string query = "SELECT * FROM PropertyPortal.Property a, PropertyPortal.PropImages b WHERE a.Prop_Id = b.Prop_ID;";
                using (MySqlCommand comma = new MySqlCommand(query, connect))
                {
                    try
                    {
                        comma.Connection.Open();
                        GetPropImages propimg = new GetPropImages();

                        read = comma.ExecuteReader();
                        while (read.Read())
                        {
                            propimg = new GetPropImages(Convert.ToInt32(read["Prop_Id"]),
                                                        Convert.ToString(read["Property_Desc"]),
                                                        Convert.ToString(read["Property_Type"]),
                                                        Convert.ToString(read["Property_Stat"]),
                                                        Convert.ToString(read["Address"]),
                                                        Convert.ToString(read["City"]),
                                                        Convert.ToInt32(read["BedRoom"]), 
                                                        Convert.ToInt32(read["BathRoom"]),
                                                        Convert.ToInt32(read["Garage"]),
                                                        Convert.ToDouble(read["Price"]),
                                                        Convert.ToInt32(read["Imgae_Id"]),
                                                        Convert.ToString(read["Name"]),
                                                        ((byte[])read["Images"]),
                                                        Convert.ToInt32(read["Prop_ID"])
                                                        );
                            propImg.Add(propimg);
                        }
                        read.Close();
                        MySqlDataReader reader = comma.ExecuteReader(System.Data.CommandBehavior.SingleRow);
                        reader.Read();
                        reader.Close();
                    }
                    catch (MySqlException ex)
                    {
                        ex.ToString();
                    }
                    finally
                    {
                        comma.Connection.Close();
                    }
                }
            }
            return propImg.ToArray();
        }

       


        /*Send an equiry email to the portal*/
        public string PostSendEmail(EmailSend email)
        {
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            NetworkCredential cred = new NetworkCredential("mokoneg11@gmail.com", "GoMo13#$");
            client.UseDefaultCredentials = false;
            client.Credentials = cred;

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(email.senderEmail);
            msg.To.Add(new MailAddress("mokoneg11@gmail.com"));
            msg.Subject = "Property Information";
            msg.IsBodyHtml = true;
            msg.Body = string.Format("<html><head><head><body><b>Name: </b>" + email.senderName + "<br /><br />" + "<b>Email: </b>" + email.senderEmail + "<br /><br />" + "<b>Mobile Number: </b>" + email.senderContact + "<br /><br />" + email.message + "</body></html>");

            try
            {
                client.Send(msg);
                return "Email Sent Successfully";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }



    }
}


