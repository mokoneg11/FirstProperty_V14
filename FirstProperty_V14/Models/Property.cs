﻿using System;
namespace FirstProperty_V14.Models
{
    public class Property
    {
        

        public int Prop_Id { get; set; }
		public string Property_Desc { get; set; }
		public string Property_Type{ get; set; }
		public string Property_Stat { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string BedRoom { get; set; }
		public string BathRoom { get; set; }
		public string Garage{ get; set; }
		public string Price { get; set; }
        public string ImgName { get; set; }
        public byte[] Images { get; set; }

		public Property(string desc, string type, string stat, string addres, string city, string bed, string bath, string garage, string amount, string imgN, byte[] pic)
		{
            Property_Desc = desc;
            Property_Type = type;
            Property_Stat = stat;
            Address = addres;
			City =  city;
            BedRoom = bed;
			BathRoom = bath;
            Garage = garage;
            Price = amount;
            ImgName = imgN;
            Images = pic;

		}
		public Property(int id, string desc, string type, string stat, string addres, string city, string bed, string bath, string garage, string amount)
		{
			Prop_Id = id;
			Property_Desc = desc;
			Property_Type = type;
			Property_Stat = stat;
			Address = addres;
			City = city;
			BedRoom = bed;
			BathRoom = bath;
			Garage = garage;
			Price = amount;
		
		}
		public Property(int id, string desc, string type, string stat, string addres, string city, string bed, string bath, string garage, string amount,string imgN, byte[] pic)
		{
            Prop_Id = id;
			Property_Desc = desc;
			Property_Type = type;
			Property_Stat = stat;
			Address = addres;
			City = city;
			BedRoom = bed;
			BathRoom = bath;
			Garage = garage;
			Price = amount;
			ImgName = imgN;
			Images = pic;
		}

        public Property()
        {
            
        }

    }
}
