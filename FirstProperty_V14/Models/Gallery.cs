﻿using System;
namespace FirstProperty_V14.Models
{
    public class Gallery
    {
        public int Gallery_Id { get; set; }
        public string File_Name { get; set; }
        public string FilePath { get; set; }


        public Gallery(int gal_id, string filename, string path)
        {
            Gallery_Id = gal_id;
            File_Name = filename;
            FilePath = path;
        }
    }
}
