﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FirstProperty_V14.Models;

namespace FirstProperty_V14.Controllers
{
    public class CustomerController : System.Web.Http.ApiController
    {
		static DataAccess acc = new DataAccess();


        //Get all Customers information stored in the database
        [HttpGet]
        [Route("api/GetCustomers")]
        public IEnumerable<Customer> GetAllCustomers()
        {
            return acc.GetAllCust();
        }


        //Login into the system
		[System.Web.Http.HttpGet]
		[System.Web.Http.Route("api/CustomersLogin")]
		public Customer GetCust(string email, string password)
		{
			return acc.CustomerLogin(email, password);
		}

        //Updating the customers information
		[System.Web.Http.HttpPut]
		[System.Web.Http.Route("api/UpdateCust")]
        public Customer UpdateCust(Customer cust, int id){
              return acc.CustomerUpdate(cust,id);
        }

        //Registering customers on the mobile application
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/Registers")]
        public string PostCust(Customer cust)
        {
            if (cust != null)
            {
                return acc.InsertUser(cust);
            }
            return "Unable to add";
        }
    }
}
