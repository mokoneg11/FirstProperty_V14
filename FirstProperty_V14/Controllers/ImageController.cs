﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using FirstProperty_V14.Models;

namespace FirstProperty_V14.Controllers
{
    public class ImageController : ApiController
    {
        static DataAccess datta = new DataAccess();

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/UploadImage")]
        public string UploadI(PropImage imgs)
        {
            int countUpload = 0;
            //PropImage imgs = new PropImage();
            //string sPath = System.Web.Hosting.HostingEnvironment.MapPath("/Images/");
            HttpFileCollection collect = System.Web.HttpContext.Current.Request.Files;
            string ur = HttpContext.Current.Request.Url.AbsoluteUri;

            for (int g = 0; g < collect.Count; g++)
            {
                System.Web.HttpPostedFile file = collect[g];
                string fileName = new FileInfo(file.FileName).Name;
               

                if (file.ContentLength > 0)
                {
                    Guid id = Guid.NewGuid();
                    string modifiedFilename = id.ToString() + " " + fileName;

                    byte[] buf = new byte[file.ContentLength];
                    file.InputStream.Read(buf, 0, file.ContentLength);

                    imgs.Name = fileName;
                    imgs.Images = buf;
                    string base64String = Convert.ToBase64String(imgs.Images, 0, buf.Length);
                    imgs.Prop_ID = Convert.ToInt32(ur);


                    //gal.File_Name = fileName;
                    //gal.FilePath = "/Images/" + modifiedFilename;

                    //if (!File.Exists(sPath + Path.GetFileName(modifiedFilename)))
                    //{
                    //     file.SaveAs(sPath + Path.GetFileName(modifiedFilename));
                    //     countUpload++;
                    //      //tblg.Image_id = new Random().Next();
                    //     //db.tblGalleries.Add(new tblGallery() { ImageUrl = id, FileName = "/Galleries/" + modifiedFilename, Title = Filename });
                    //}

                    return base64String;
                }
            }
            return datta.AddImage(imgs);
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetProp")]
        public IEnumerable<PropImage> getImages(){
       
            return datta.GetImages();
        }
    }
}
