﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FirstProperty_V14.Models;

namespace FirstProperty_V14.Controllers
{
    public class PropImaController : System.Web.Http.ApiController
    {
        DataAccess data = new DataAccess();

        [HttpGet]
        [Route("api/GetAllProperty")]
        public IEnumerable<GetPropImages> GetAllProperties()
        {
            return data.GetAll();
        }
    }
}
