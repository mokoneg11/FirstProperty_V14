﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using FirstProperty_V14.Models;

namespace FirstProperty_V14.Controllers
{
    public class PropertyController : System.Web.Http.ApiController
    {
        static DataAccess data = new DataAccess();

        [HttpPost]
        [Route("api/AddProperty")]
        public string AddProperty(Property prop){
            if (prop != null)
            {
                
                string sPath = System.Web.Hosting.HostingEnvironment.MapPath("/Images/");
                HttpFileCollection collect = System.Web.HttpContext.Current.Request.Files;
                string ur = HttpContext.Current.Request.Url.AbsoluteUri;

                for (int g = 0; g < collect.Count; g++)
                {
                    System.Web.HttpPostedFile file = collect[g];
                    string fileName = new FileInfo(file.FileName).Name;

                    if (file.ContentLength > 0)
                    {
                        byte[] buf = new byte[file.ContentLength];
                        file.InputStream.Read(buf, 0, file.ContentLength);
                        prop.ImgName = fileName;
                        prop.Images = buf;
                        string base64String = Convert.ToBase64String(prop.Images, 0, prop.Images.Length);
                        prop.Prop_Id = Convert.ToInt32(ur);
                        return base64String;
                    }
                }
                return data.InsertProperty(prop);
            }
            return "Failed to add property";
        }

        [HttpGet]
        [Route("api/GetProperty")]
        public IEnumerable<Property> GetAllProperties()
        {
            return data.GetProperty();
        }

        [HttpPut]
        [Route("api/PutProperty")]
        public Property PutProp(int id,Property prope)
        {
            return data.PropertyUpdate(id, prope);
        }

    }
}
